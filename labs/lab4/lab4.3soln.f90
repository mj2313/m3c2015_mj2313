! Lab 4, task 3

program task3
    implicit none
    integer :: i1
    integer, parameter :: N=5
    real(kind=8) :: sum
    integer :: sum2

    sum = 0.d0
    sum2 = 0

!Add a do-loop which sums the first N odd integers and prints the result
    do i1 = 1,N
        sum = sum + 2.d0*dble(i1)-1.d0 !illustrates requirements for double precision
        sum2 = sum2 + 2*i1-1 !better for this problem
    end do

    print *, 'sum = ',sum

    print *, 'sum2 = ',sum2



end program task3

! To compile this code:
! $ gfortran -o task3.exe lab4.3.f90
! To run the resulting executable: $ ./task3.exe
