!statsmod_soln.f90
!module containing routines to compute the mean and variance of an array
module stats
    implicit none
    integer :: N !size of array (set in calling program)
    real(kind=8), allocatable, dimension(:) :: f !array set in calling program
    save
contains

subroutine initialize_stats()
    !Read in N from input file, data.in
    !Allocate the size of f accordingly
    implicit none

    open(unit=10,file='data.in')
        read(10,*) N
    close(10)
    allocate(f(N))

end subroutine initialize_stats

function compute_mean()
!Compute mean using f and N
    implicit none
    real(kind=8) :: compute_mean

    compute_mean = sum(f)/dble(N)

end function compute_mean

subroutine compute_var(fmean,fvar)
!Compute variance using f,N, and input variable fmean
!Return the variance via the output variable, fvar
!Variable declarations for fmean and fvar will need to be added
    implicit none
    real(kind=8), intent(in) :: fmean
    real(kind=8), intent(out) :: fvar

    fvar = sum((f-fmean)**2)/dble(N)

end subroutine compute_var

end module stats
