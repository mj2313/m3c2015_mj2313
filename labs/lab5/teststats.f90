!Test code for statsmod
!To compile: gfortran -o teststats.exe statsmod.f90 teststats.f90
!To run: ./teststats.exe
program test
    use stats
    implicit none
    integer :: i1
    real(kind=8) :: meanf,varf


    call initialize_stats()

    do i1=1,N
        f(i1)=dble(i1)
    end do

    print *, 'f=',f

    meanf = compute_mean()

    call compute_var(meanf,varf)

    print *, 'mean=',meanf
    print *, 'var=',varf

    deallocate(f)
end program test
