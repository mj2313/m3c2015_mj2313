#### The repo should also contain a readme file with a list of all files in the repo that you created or modified and a very brief description of each file.

#Folders and their files in my m3c 
project:

##Part 1
####	fdmodule.f90
####	p1_3.py
####    fdmodule2d.f90 (added from the Homework 4 Solution)

##Part 2
####	fdmodule.f90
####	p2.py
 
##Part 3
####	advmodule.f90
####	fdmoduleB.f90
####	ode.f90
####	ode_mpi.f90
####	p3.py
####	p3_v2.py
 
##Part 4
####	advmodule2d.f90
####	fdmoduleB.f90
####	ode2d.f90
####	p4.py
