#Project part 4
import numpy as np
import matplotlib.pyplot as plt
import adv2d

#My additional imports
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter

#gfortran -c advmodule2d.f90
#f2py -llapack -c advmodule2d.f90 fdmoduleB.f90 fdmodule2dp.f90 ode2d.f90 -m adv2d --f90flags='-fopenmp' -lgomp 
#Note:I changed ode2d.f90 to ode2d_v2.f90 in my execution command

c1 = 1.; c2 = 1.; t = 1.;





#add functions as needed

"""
 The accuracy of advection1 will be assessed in test_advection1. Compute the (scaled) L1 error with t=1, c=1, and S=1 for a range of values of n (keeping dx=1.0n). Make a plot illustrating the convergence rate. Quantitatively estimate the convergence rate and return that estimate. Include an assert statement that requires that the computed convergence rate is within 5% of the analytical result and which prints an error message if it is not. You should choose your values of n so that the assert condition is satisfied.
"""

#----------------------------------------------
def advection2d(t,n1,n2,dx,dy,c1=1.0,c2=1.0,S=1.0,par=0,numthreads = 1, display=False):
    """solve 2d advection eqn, df/dt + c_1 df/dx +c_2 df/dx = S 
    where c_1,c_2 and S are constants, x=0,dx,...(N-1)*dx, y=0,dy,...(N-1)*dy,
    and f is periodic in x and y,
    Initial condition: f(x,y,t=0) = exp(-100.0*((x-x0)**2+(y-y0)**2)), where x0 = y0 = 0.5
    Returns: f(x,y,t) (t is fixed)
    """


    #construct grid and initial condition, set time span for odeint
    x = np.linspace(0,(n1-1)*dx,n1)
    y = np.linspace(0,(n2-1)*dy,n2)
    xx, yy = np.meshgrid(x, y, sparse=True)
    f0 = np.exp(-100.0*((xx-0.5)**2+(yy-0.5)**2))
    nt =3200
    dt = t/float(nt)

    adv2d.ode2d.numthreads = numthreads
    adv2d.ode2d.par = par

    adv2d.advmodule.s_adv = S
    adv2d.advmodule.c1_adv = c1
    adv2d.advmodule.c2_adv = c2

    y = adv2d.ode2d.rk4(t,f0,dt,nt)

    #display results
    if display:
        plt.figure()
        ax = fig.gca(projection='3d')
        xx, yy = np.meshgrid(x, y, sparse=True)

    
        surf = ax.plot_surface(xx, yy, y[0], rstride=1, cstride=1, cmap=cm.coolwarm,linewidth=0, antialiased=False)
    
        ax.zaxis.set_major_locator(LinearLocator(20))
        ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
        ax.set_zlim3d(t, t+1)
        fig.colorbar(surf, shrink=0.5, aspect=10)
        plt.xlabel('x')
        plt.ylabel('y')
        plt.ylabel('f')
        plt.title('Maros Janco, advection2d advection eqn. \n solution for t,n1,n2,dx,dy,c1,c2,S=%d,%2.3f,%2.1f,%2.1f,%2.1f,%2.1f,%2.1f,%2.1f' %(t,n1,n2,dx,dy,c1,c2,S))
        plt.grid()
        plt.axis('tight')
    return y[0]
#copied text from hw4 solution, will be changed soon
"""
#----------------------------------------------  
def test_advection1():

    Call advection1 with a range of values of n and
    parameters, t=1,c=1,S=1,dx=1/n
    Compute and plot error, estimate convergence rate
    and require "close" agreement with expected rate
    
    #set problem parameters
    nvalues = np.array([100, 200, 400, 800])
    t = 1
    c = 1.0
    S=1.0
       
    error = []  
    for n in nvalues:
        
            #for each n, construct grid and homogeneous part of exact solution            
            dx = 1.0/(n)
            x = np.arange(0.0,n*dx,dx)
            x0 = 0.5*x[-1]
            f0 = np.exp(-100.0*(x-x0)**2) 

        #f_analytic = S*t + np.exp(-100.0*((((xx-c1*t)-0.5)**2+((yy-c2*t)-0.5)**2))
        
    #solve advection eqn and compute error            
            f = advection1(t,n,dx,c=1.0,S=1.0,display=False)                            
            error = error + [np.mean(np.abs(f-(f0+S*t)))]
   
    #compute convergence rate
    m,p = np.polyfit(np.log(nvalues),np.log(error),1)

    #display results
    plt.figure()
    plt.loglog(nvalues,error)
    plt.loglog(nvalues,np.exp(p)*nvalues**m,'--')
    plt.legend(('computed','lsq linear fit'),loc='best')
    plt.xlabel('n')
    plt.ylabel('error')
    plt.title('Prasun Ray, test_advection1, variation of error with n for c=S=1, dx=1/n')
    plt.axis('tight')
 
    #Check accuracy of convergence rate   
    assert abs(m+2)<0.05, "error, convergence rate should be 2, computed value is -%2.3f"%(m)

    return -m,error


    h = plt.contourf(x,y,z)
"""
if __name__=='__main__':
    #add code as needed

    n1 = 100
    n2 = 200
    dx = 1.0/float(n1) ### or n1-1 
    dy = 1.0/float(n2)
    t = 2.
    
    f = advection2d(t,n1,n2,dx,dy)

    print S

    #For calling the variables in the plot title if they are not specified:
    #if c1 is None: c1 = 1.0
    #if c2 is None: c2 = 1.0
    #if S is None: S = 1.0
    #if par is None: par = 0
    #if numthreads is None: numthreads = 1

    #print S
    print f.shape

    #plot of the initial conditions
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    
    x = np.linspace(0,(n1-1)*dx,n1)
    y = np.linspace(0,(n2-1)*dy,n2)
    xx, yy = np.meshgrid(x, y, sparse=True)
    f0 = np.exp(-100.0*((xx-0.5)**2+(yy-0.5)**2))
    
    surf = ax.plot_surface(xx, yy, f0, rstride=1, cstride=1, cmap=cm.coolwarm,
                           linewidth=0, antialiased=False)
    
    ax.zaxis.set_major_locator(LinearLocator(20))
    ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
    ax.set_zlim3d(0, 1)
    fig.colorbar(surf, shrink=0.5, aspect=10)
    plt.xlabel('x')
    plt.ylabel('y')
    plt.ylabel('f')
    plt.title('Maros Janco, advection2d advection eqn. \n solution for t,n1,n2,dx,dy,c1,c2,S=%d,%2.3f,%2.1f,%2.1f,%2.1f,%2.1f,%2.1f,%2.1f' %(t,n1,n2,dx,dy,c1,c2,S))
    plt.grid()
    plt.axis('tight')
    
    plt.show()
    
    #plot of 2d advection
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    
    x = np.linspace(0,(n1-1)*dx,n1)
    y = np.linspace(0,(n2-1)*dy,n2)
    xx, yy = np.meshgrid(x, y, sparse=True)
    #f_analytic = S*t + np.exp(-100.0*((((xx-c1*t)-0.5)**2+((yy-c2*t)-0.5)**2))
    
    surf = ax.plot_surface(xx, yy, f, rstride=1, cstride=1, cmap=cm.coolwarm,
                           linewidth=0, antialiased=False)
    
    ax.zaxis.set_major_locator(LinearLocator(20))
    ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
    ax.set_zlim3d(t, t+1)
    fig.colorbar(surf, shrink=0.5, aspect=10)
    
    plt.show()

