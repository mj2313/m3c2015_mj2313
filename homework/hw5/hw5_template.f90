!To compile: mpif90 -O3 -o diffm.exe hw5_template.f90
!To run with two processes: mpiexec -n 2 diffm.exe
!Your code should work with any number of processes assuming
!there are more grid points than processes.
!  1< numprocs < 2∗ntotal
! Maros Janco 00866730
!----------------------------------------------------------------------
program diff_mpi
    use mpi
	implicit none
	integer :: i1,j1,Ntotal,Nlocal,istart,iend
    double precision :: dx,buffer,df_max
    double precision, allocatable, dimension(:) :: x,f,f4,df,df4
    integer :: myid, numprocs, ierr
    double precision :: df_max_proc

!---------------------------------------------------------------
    !Set up calculation
!---------------------------------------------------------------
    ! Initialize MPI
    call MPI_INIT(ierr)
    call MPI_COMM_RANK(MPI_COMM_WORLD, myid, ierr)
    call MPI_COMM_SIZE(MPI_COMM_WORLD, numprocs, ierr)


	!read data from data.in
	open(unit=10, file='data.in')
        read(10,*) Ntotal
	close(10)

    !construct decomposition: assign Nlocal points from istart to iend  to each processor
    call MPE_DECOMP1D( Ntotal, numprocs, myid, istart, iend)
    Nlocal = iend - istart + 1
    allocate(x(Nlocal),f(0:Nlocal+1),df(Nlocal),df4(Nlocal),f4(-1:Nlocal+2)) !set dimensions of x,f,df
    !**NOTE** f4 must be allocated (DONE)

    !make grid and field
    call make_grid(Ntotal,Nlocal,istart,iend,x)
    dx = x(2)-x(1)
    print *, 'proc', myid, ' has been assigned the interval x=', x(1),x(Nlocal)

    call make_field(Nlocal,x,f(1:Nlocal)) !note: f(0) and f(Nlocal+1) must be obtained from neighboring processors
    print *, f,"**"
    !*** initialize f4 as appropriate (DONE)
    f4(1:Nlocal) = f
!--------------------------------------------------------------------
    !Now, compute derivative, df/dx with 2nd and 4th order fd schemes
!--------------------------------------------------------------------
    call fd2_mpi(MPI_COMM_WORLD,myid,numprocs,nlocal,dx,f,df)

      df_max_proc = maxval(abs(df))
      
      call MPI_REDUCE(df_max_proc, df_max, 1, MPI_DOUBLE_PRECISION,MPI_MAX,0,MPI_COMM_WORLD,ierr)

!hw5: compute max(|df|) and store as df_max on myid=0
    if (myid==0) print *, 'max(|df|) = ',df_max


    call fd4_mpi(MPI_COMM_WORLD,myid,numprocs,nlocal,dx,f4,df4)


!---------------------------------------------------------------
    !Output df,df4
!---------------------------------------------------------------
    call output(MPI_COMM_WORLD,myid,numprocs,nlocal,ntotal,dx,df)
    call parallel_output(MPI_COMM_WORLD,myid,numprocs,nlocal,istart,dx,df4)

    deallocate(x,f,f4,df)
    call MPI_FINALIZE(ierr)

end program diff_mpi
!--------------------------------------------------------------------
!--------------------------------------------------------------------
!--------------------------------------------------------------------
!--------------------------------------------------------------------
!subroutine output()
!
! Gather df on myid=0, and output in python-friendly format
!---------------------------------------------------------------

subroutine output(comm,myid,numprocs,nlocal,ntotal,dx,df)
    use mpi
    implicit none
    integer, intent(in) :: comm,myid,numprocs,nlocal,ntotal
    double precision, dimension(nlocal), intent(in) :: df
    double precision, allocatable, dimension(:) :: df_total
    real(kind=8) :: dx
    integer :: i1,ierr

!for gather:
    integer, allocatable, dimension(:) :: Nper_proc, disps


    allocate(df_total(Ntotal),Nper_proc(Numprocs),disps(Numprocs))

    !gather Nlocal from each proc to array Nper_proc on myid=0
    call MPI_GATHER(Nlocal,1,MPI_INT,Nper_proc,1,MPI_INT,0,comm,ierr)
    if (myid==0) then
        print *,'Nper_proc=',Nper_proc
        disps(1)=0
        do i1=2,Numprocs
            disps(i1) = disps(i1-1)+Nper_proc(i1-1) !needed for gatherv below
        end do
        print *, 'disps=', disps
    end if

    !collect df from each processor onto myid=0
    call MPI_GATHERV(df,Nlocal,MPI_DOUBLE_PRECISION,df_total,Nper_proc, &
                disps,MPI_DOUBLE_PRECISION,0,comm,ierr)

    !output
    if (myid==0) then
        open(unit=23,file='field.dat')
        do i1=1,Ntotal
            write(23,*) dx*dble(i1-1),df_total(i1)
        end do
        close(23)
    end if

    !to plot in python:
    !f=np.loadtxt('field.dat')
    !plt.figure()
    !plt.plot(f[0,:],f[1,:])

end subroutine output

!--------------------------------------------------------------------
!subroutine parallel_output()
!
! output df in python-friendly format without gathering full 
! fields on single process
!---------------------------------------------------------------

subroutine parallel_output(comm,myid,numprocs,nlocal,istart,dx,df)
    use mpi
    implicit none
    integer, intent(in) :: comm,myid,numprocs,nlocal,istart
    double precision, dimension(nlocal), intent(in) :: df
    real(kind=8) :: dx
    integer :: i1,j1,ierr
    integer, dimension(MPI_STATUS_SIZE) :: status


    if (myid==0) then ! master process
        !create file and write out own copy of data
        open(unit=23,file='pfield.dat')
        do i1=1,nlocal
            write(23,*) dx*dble(i1-1),df(i1)
        end do
        close(23)

        !To be completed for hw5


    else !worker processes

        !To be completed for hw5

        open(unit=23,file='pfield.dat',position='append')
        write(23,*) !complete as appropriate

        !To be completed for hw5


    end if
    !to plot in python:
    !f=np.loadtxt('pfield.dat')
    !plt.figure()
    !plt.plot(f[0,:],f[1,:])

end subroutine parallel_output


!--------------------------------------------------------------------
!--------------------------------------------------------------------
!subroutine fd2_mpi
!
!Compute derivative of periodic function using second-order finite differences
!using mpi to exchange boundary data with other processes
!**Note** The code shown in lecture had f allocated with dimension(1:nlocal+2) 
!so the send/recv statements were a little different
subroutine fd2_mpi(comm,myid,numprocs,nlocal,dx,f,df)
    use mpi
    implicit none
    integer, intent(in) :: comm,myid,numprocs,nlocal
    real(kind=8), intent(in) :: dx
    real(kind=8), dimension(0:nlocal+1),intent(in) :: f
    real(kind=8), dimension(nlocal) :: df
    integer, dimension(MPI_STATUS_SIZE) :: status
    integer :: sender,receiver,ierr
!-----------------------------------------------------------
!Send data at top boundary up to next processor
!i.e. send f(nlocal) to be received by myid+1 and store it there as f(0)
!data from myid=numprocs-1 is sent to myid=0
!-----------------------------------------------------------
    if (myid<numprocs-1) then
        receiver = myid+1
    else 
         receiver = 0
    end if

    if (myid>0) then
        sender = myid-1
    else
        sender = numprocs-1
    end if

    call MPI_SEND(f(nlocal),1,MPI_DOUBLE_PRECISION,receiver,0,comm,ierr) !send f(nlocal) of size 1 of tag 0  !send to ! receive from
    call MPI_RECV(f(0),1,MPI_DOUBLE_PRECISION,sender,MPI_ANY_TAG,comm,status,ierr)

    call MPI_BARRIER(comm,ierr)
    !print *, "1", nlocal+2, "aaaaaaaaaa"
    !print *, f
!-----------------------------------------------------------
!Send data at bottom boundary down to previous processor
!
!-----------------------------------------------------------
   
    if (myid>0) then
        receiver = myid-1
    else
        receiver = numprocs-1
    end if

    if (myid<numprocs-1) then
        sender = myid+1
    else 
         sender = 0
    end if



    call MPI_SEND(f(0),1,MPI_DOUBLE_PRECISION,receiver,0,comm,ierr) 
    call MPI_RECV(f(nlocal),1,MPI_DOUBLE_PRECISION,sender,MPI_ANY_TAG,comm,status,ierr)

    call MPI_BARRIER(comm,ierr)
    !print *, "2", nlocal+2, "rrrrrrrrrrrrr"
  
    !print *, f
   !----finished sending/receiving
    df = (f(2:Nlocal+1) - f(0:Nlocal-1))/(2.d0*dx) !approximate derivative
end subroutine fd2_mpi

!--------------------------------------------------------------------
!Compute derivative of periodic function using fourth-order finite differences
!using mpi to exchange boundary data with other processes
!-------------------
subroutine fd4_mpi(comm,myid,numprocs,nlocal,dx,f4,df)
    use mpi
    implicit none
    integer, intent(in) :: comm,myid,numprocs,nlocal
    real(kind=8), intent(in) :: dx
    real(kind=8), dimension(nlocal) :: df
    integer, dimension(MPI_STATUS_SIZE) :: status
    integer :: sender,receiver,ierr
    real(kind=8) :: invtwelvedx
    real(kind=8), dimension(-1:nlocal+2),intent(in) :: f4

    !**Note:** f4 must be declared

!To be completed for hw5

!-----------------------------------------------------------
!Send data at top boundary up to next processor
!i.e. send f(nlocal) to be received by myid+1 and store it there as f(0)
!data from myid=numprocs-1 is sent to myid=0
!-----------------------------------------------------------
    if (myid<numprocs-1) then
        receiver = myid+1
    else 
         receiver = 0
    end if

    if (myid>0) then
        sender = myid-1
    else
        sender = numprocs-1
    end if

    call MPI_SEND(f4(nlocal-1:nlocal),2,MPI_DOUBLE_PRECISION,receiver,0,comm,ierr)
    call MPI_RECV(f4(-1:0),2,MPI_DOUBLE_PRECISION,sender,MPI_ANY_TAG,comm,status,ierr)

    call MPI_BARRIER(comm,ierr)



!-----------------------------------------------------------
!Send data at bottom boundary down to previous processor
!
!-----------------------------------------------------------
   
    if (myid>0) then
        receiver = myid-1
    else
        receiver = numprocs-1
    end if

    if (myid<numprocs-1) then
        sender = myid+1
    else 
         sender = 0
    end if

    call MPI_SEND(f4(-1:0),2,MPI_DOUBLE_PRECISION,receiver,0,comm,ierr)
    call MPI_RECV(f4(nlocal-1:nlocal),2,MPI_DOUBLE_PRECISION,sender,MPI_ANY_TAG,comm,status,ierr)

    call MPI_BARRIER(comm,ierr)


    !----finished sending/receiving

    df = (-(f4(3:Nlocal+2) - f4(-1:Nlocal-2)) + 8 * (f4(2:Nlocal+1) - f4(0:Nlocal-1)))/(12.d0*dx) !approximate derivative



end subroutine fd4_mpi

!--------------------------------------------------------------------
!---------------------------------------------------------------------
!subroutine make_grid
!
!Let xtotal be a uniformly spaced grid from 0 to 1 with Ntotal+1 points
!This subroutine generates x = xtotal(istart:iend)
!---------------------------------------------------------------------
subroutine make_grid(Ntotal,Nlocal,istart,iend,x)
    implicit none
    integer :: i1
    integer, intent(in) :: Ntotal,Nlocal,istart,iend
    double precision, dimension(Nlocal), intent(out) :: x

    do i1 = istart,iend
        x(i1-istart+1) = dble(i1-1)/dble(Ntotal)
    end do

end subroutine make_grid
!--------------------------------------------------------------------

!---------------------------------------------------------------------
!subroutine make_field
!generates a simple sinusoidal function, f(x)
!input: N, size of field
!       x, grid points
!---------------------------------------------------------------------
subroutine make_field(N,x,f)
    implicit none
    real(kind=8), parameter :: pi = acos(-1.d0)
    integer, intent(in) :: N
    double precision, dimension(N), intent(in) :: x
    double precision, dimension(N), intent(out) :: f


    f = sin(2.d0*pi*x) 

end subroutine make_field

!--------------------------------------------------------------------
!  (C) 2001 by Argonne National Laboratory.
!      See COPYRIGHT in online MPE documentation.
!  This file contains a routine for producing a decomposition of a 1-d array
!  when given a number of processors.  It may be used in "direct" product
!  decomposition.  The values returned assume a "global" domain in [1:n]
!
subroutine MPE_DECOMP1D( n, numprocs, myid, s, e )
    implicit none
    integer :: n, numprocs, myid, s, e
    integer :: nlocal
    integer :: deficit

    nlocal  = n / numprocs
    s       = myid * nlocal + 1
    deficit = mod(n,numprocs)
    s       = s + min(myid,deficit)
    if (myid .lt. deficit) then
        nlocal = nlocal + 1
    endif
    e = s + nlocal - 1
    if (e .gt. n .or. myid .eq. numprocs-1) e = n

end subroutine MPE_DECOMP1D





