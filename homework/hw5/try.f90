program try

	implicit none
	integer :: i1
        double precision, dimension(-1:1) :: f
    !double precision :: df_max_proc

!---------------------------------------------------------------
    !Set up calculation
!---------------------------------------------------------------
    ! Initialize MPI
do i1 = -1,1
    f(i1) = i1*2
end do 

print *, f
end program try
