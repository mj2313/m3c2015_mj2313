"""Assess accuracy and perfomance of fortran finite difference module, fdmodule.
Converted to python module fd.so using f2py, i.e.
by executing 'f2py -llapack -c hw3.f90 -m fd' in Unix terminal.
"""

from fd import fdmodule as f1
import numpy as np
import matplotlib.pyplot as plt

def fdtest_a(alpha=100.0):
	"""alpha: input variable which sets width of 
    Guassian function to be differentiated
m: output two-element array, where the first element (m[0]) is such that the error with respect to the 2nd order centered finite difference (FD) scheme is proportional to the step-size dx to the power of m[0]. Similarly, the second element (m[1]) of m is such that the error with respect to the 4th order finite difference scheme is proportional to the step-size dx to the power of m[1]. The way the two elements of m are mathematically defined/estimated is by polynomial curve fitting (particularly linear fitting, in python via np.polyfit()), that is a linear least-squares fit is applied to the log(error) vs. log(dx) curve, and then we take the coefficient of the linear term. If we worked with 2nd order centered FD scheme, this coefficient is m[0], if we worked with 4th order FD scheme, it is m[2]. This is basically the same approach as used in the previous m3c coursework (HW 2, Part 2, 3)."""

	N = np.array([100.,200,400,800,1600,3200,12800])
	DX = 2./(N-1)
	error = np.empty((2, len(N)))
	m = np.empty(2)

	for i in range(len(N)):
		f1.n = N[i]
		f1.dx = DX[i]
		error[:,i] = f1.test_fd(alpha)

	plt.figure()
	plt.loglog(N, error[0,:], color='blue', label="2$^{nd}$ order centered FD scheme", linestyle='-')  # blue 2nd order
	plt.loglog(N, error[1,:], color='red', label="4$^{th}$ order FD scheme", linestyle='-')   # red 4th order
	plt.title("Maros Janco, fdtest_a, Loglog plot of error(n) vs. n,\n" +
	"Accuracy testing of fdmodule with Gaussian, alpha = %s"%(alpha),  y=1.02)
	plt.xlabel('n')
	plt.ylabel('error')
	plt.legend(loc=1, prop={'size':12})

	(m[0], est2) = np.polyfit(np.log(DX),np.log(error[0,:]),1)    # 2nd order
	(m[1], est2) = np.polyfit(np.log(DX),np.log(error[1,:]),1)    # 4th order
    
	return error, m
    
def fdtest_at(alpha=100.0):
	"""alpha: input variable which sets width of 
    Guassian function to be differentiated
    """ 

	N = np.array([100.,200,400,800,1600,3200,12800])
	DX = 2./(N-1)
	error = np.empty((2, len(N)))
	walltime = np.empty((2, len(N)))
	m = np.empty(2)


	for i in range(len(N)):
		f1.n = N[i]
		f1.dx = DX[i] 
		error[:,i] = f1.test_fd_time(alpha)[0]
		walltime[:,i] = f1.test_fd_time(alpha)[1]

	plt.figure(figsize=(15,6))
	# error vs n
	plt.subplot(121)
	plt.loglog(N, error[0,:], color='blue', label="2$^{nd}$ order centered FD scheme", linestyle='-')  # blue 2nd order
	plt.loglog(N, error[1,:], color='red', label="4$^{th}$ order FD scheme", linestyle='-')   # red 4th order
	plt.title("Maros Janco, fdtest_at, Loglog plot of error(n) vs. n,\n" +
	"Accuracy testing of fdmodule with Gaussian, alpha = %s"%(alpha), y=1.02)
	plt.xlabel('n')
	plt.ylabel('error')
	plt.legend(loc=1, prop={'size':12})

	# walltime vs n
	plt.subplot(122)
	plt.loglog(N, walltime[0,:], color='blue', label="2$^{nd}$ order centered FD scheme", linestyle='-.')  # blue 2nd order
	plt.loglog(N, walltime[1,:], color='red', label="4$^{th}$ order FD scheme", linestyle='-.')   # red 4th order
	plt.title("Maros Janco, fdtest_at, Loglog plot of walltime(n) vs. n,\n" + 
	"Speed testing of fdmodule with Gaussian, alpha = %s"%(alpha), y=1.02)
	plt.xlabel('n')
	plt.ylabel('walltime')
	plt.legend(loc=2, prop={'size':12})
    
	#estimated exponents of delta x
	(m[0], est2) = np.polyfit(np.log(DX),np.log(error[0,:]),1)    # 2nd order
	(m[1], est2) = np.polyfit(np.log(DX),np.log(error[1,:]),1)    # 4th orders
    
	return error, m, walltime

def fdtest_eff(alpha=100.):
	"""The output is a loglog plot of error vs. walltime with respect to both 2nd and 4th order finite difference schemes (so 2 curves for both FD schemes are shown respectively). The way we can choose which FD method to use is the following: 
1. Consider firstly a specific error (accuracy) of your interest.
2. Find the walltimes of both 2nd centered and 4th order FD schemes, that correspond to this error.
3. The more effecient method for this error specification is such a FD scheme, which has the lower walltime than the other FD scheme (i.e. is computationally faster with the same accuracy).
Note: If only one FD scheme has a walltime(s) corresponding to the requisite error, it means that this scheme only can produce such error/accuracy so we obviously choose this method given a list of different number of points N, which, I think in my case, produces a sufficient range one could practically consider.
"""
	N = np.array([10.,100,200,400,1600,6400,25600,102400,409600])
	DX = 2./(N-1)
	error = np.empty((2, len(N)))
	walltime = np.empty((2, len(N)))
	
	for i in range(len(N)):
		f1.n = N[i]
		f1.dx = DX[i] 
		error[:,i] = f1.test_fd_time(alpha)[0]
		walltime[:,i] = f1.test_fd_time(alpha)[1]

	plt.figure()
	plt.loglog(walltime[0,:], error[0,:], color='blue', label="2$^{nd}$ order centered FD scheme", linestyle='-') # blue 2nd order
	plt.loglog(walltime[1,:], error[1,:], color='red', label="4$^{th}$ order FD scheme", linestyle='-') # red 4th order
	plt.title("Maros Janco, fdtest_eff, Loglog plot of error vs. walltime,\n" +
	"Efficiency testing of fdmodule with Gaussian, alpha = %s"%(alpha), y=1.02)
	plt.xlabel('walltime')
	plt.ylabel('error')
	plt.legend(loc=1, prop={'size':12})
        plt.savefig("hw3.png")
    
#This section will be used for assessment, you may enter a call to fdtest_eff, but
#the three un-commented lines should be left as is in your final submission.    
if __name__ == "__main__":
	e1,m=fdtest_a(alpha=125.0)
	e2,m,t2 = fdtest_at()
	#add function call to fdtest_eff here if needed
	fdtest_eff()
	plt.show()
