!program to test fdmodule in hw3_dev.f90
!gfortran -o test1.exe hw3play.f90 test_hw3_template.f90 -llapack
!./test1.exe

program test_fdmodule
    use fdmodule
    !For this code to work, the user must:
    !1. add variable declarations, alpha (double prec),
    !and error (array of double prec.)
    !2. tell the code where it can find information on dx, N, and test_fd
    implicit none
    !integer, parameter  :: N = 5
    real*8 :: alpha
    real*8, dimension(2) :: error, time
    open(unit=10,file='datahw3.in')
        read(10,*) n
        read(10,*) alpha
    close(10)

!If you follow the instructions below the header, you will be able to use this code to call test_fd and check your work thus far. This is not required for the assignment.
    dx = 2.d0/dble(n-1)

    !call test_fd(alpha,error)

    call test_fd_time(alpha,error,time)
    
    print *, 'n=',n
    print *, 'dx=',dx
    print *, 'error fd2, cfd4 =',error(:)
    print *, 'time fd2, cfd4 =',time(:)

end program test_fdmodule
