!Module containing routines for differentiating array of size N with
!2nd order and 4th order compact finite differences
!Test functions apply these methods to a Gaussian function and
!return performance information.

module fdmodule
    implicit none
    integer :: n        !number of grid points
    real(kind=8) :: dx  !grid spacing
    save

contains
!-------------------
subroutine fd2(f,df)
    !2nd order centered finite difference
    !Header variables
    !f: function to be differentiated
    !df: derivative of f
    implicit none
    real(kind=8), dimension(:), intent(in) :: f
    real(kind=8), dimension(size(f)), intent(out) :: df
    integer :: i

    df(1) = 0.5d0*(f(2)-f(n))/dx
    df(n) = 0.5d0*(f(1)-f(n-1))/dx

    do i = 2,n-1
        df(i) = 0.5d0*(f(i+1)-f(i-1))/dx
    end do

end subroutine fd2
!-----------------

subroutine cfd4(f,df)
    !compact 4th order centered finite difference
    !Header variables
    !f: function to be differentiated
    !df: derivative of f
    implicit none
    real(kind=8), dimension(:), intent(in) :: f
    real(kind=8), dimension(size(f)), intent(out) :: df
    integer :: LDB,NRHS,INFO,i
    real(kind=8), allocatable, dimension(:) :: DL, DU, D
    real(kind=8), allocatable, dimension(:,:) :: B,Btemp

    LDB = n
    NRHS = 1

    allocate(B(LDB,NRHS),DL(n-1),D(n),DU(n-1))

    DL = (/ (1.d0, i=1,n-1) /)
    DU = DL
    DU(1) = 2.d0
    DL(n-1) = 2.d0
    D = (/ (4.d0, i=1,n) /)
    D(1) = 1.d0
    D(n) = 1.d0

    B(1,1) = (-2.5d0*f(1) + 2.d0*f(2) + 0.5d0*f(3))/dx
    B(n,1) = (2.5d0*f(n) - 2.d0*f(n-1) - 0.5d0*f(n-2))/dx

    do i =2,n-1
     B(i,1) = 3.d0*(f(i+1)-f(i-1))/dx
    end do
    
    Btemp = B
    call dgtsv(n, NRHS, DL, D, DU, Btemp, LDB, INFO)

    df = Btemp(:,1)

end subroutine cfd4
!------------------

subroutine test_fd(alpha,error)
    !test accuracy of fd2 and cfd4 with a Gaussian function, f=exp(-alpha*(x-x0)**2)
    !Header variables:
    !alpha: sets width of Gaussian
    !error: 2-element array containing fd2 and cfd4 approximation errors
    implicit none
    integer :: i1
    real(kind=8), intent(in) :: alpha
    real(kind=8), intent(out) :: error(2)
    real(kind=8), dimension(n) :: x, fg, dfg, df2,df4
    real(kind=8) :: x0

    x = (/ ((dble(i1)-1.d0)*dx, i1=1,n) /)
    x0 = 0.5d0*maxval(x)
    do i1 = 1,n
        fg(i1) = exp(-alpha*(x(i1)-x0)**(2.d0))
        dfg(i1) = -2.d0*alpha*(x(i1)-x0)*fg(i1)
    end do

    call fd2(fg,df2)
    error(1) = sum(abs(dfg-df2))/dble(n)    !scaled L_1 norm w.r.t. df2
    call cfd4(fg,df4)
    error(2) = sum(abs(dfg-df4))/dble(n)    !scaled L_1 norm w.r.t. cfd4

end subroutine test_fd
!---------------------

subroutine test_fd_time(alpha,error,time)
    !test accuracy and speed of fd2 and cfd4 with a Gaussian function, f=exp(-alpha*(x-x0)**2)
    !Header variables:
    !alpha: sets width of Gaussian
    !error: 2-element array containing fd2 and cfd4 approximation errors
    !time: 2-element array containing wall-clock time required by 1000 calls
    !to fd2 and cfd4
    implicit none
    real(kind=8), intent(in) :: alpha
    real(kind=8), intent(out) :: error(2),time(2)
    real(kind=8), dimension(n) :: x, fg, dfg, df2, df4
    integer :: i
    real(kind=8) :: x0
    integer(kind=8) :: clock_fd2_t1, clock_fd2_t2, clock_rate_fd2
    integer(kind=8) :: clock_cfd4_t1, clock_cfd4_t2, clock_rate_cfd4

    x = (/ ((dble(i)-1.d0)*dx, i=1,n) /)
    x0 = 0.5d0*maxval(x)
    do i = 1,n
        fg(i) = exp(-alpha*(x(i)-x0)**(2.d0))
        dfg(i) = -2.d0*alpha*(x(i)-x0)*fg(i)
    end do

    call system_clock(clock_fd2_t1)
    do i = 1,1000
        call fd2(fg,df2)
    end do
    call system_clock(clock_fd2_t2,clock_rate_fd2)
    time(1) = dble(clock_fd2_t2-clock_fd2_t1)/dble(clock_rate_fd2)  !elapsed wall time for 1000 calls of fd2 (seconds)
    error(1) = sum(abs(dfg-df2))/dble(n)

    call system_clock(clock_cfd4_t1)
    do i = 1,1000
        call cfd4(fg,df4)
    end do
    call system_clock(clock_cfd4_t2,clock_rate_cfd4)
    time(2) = dble(clock_cfd4_t2-clock_cfd4_t1)/dble(clock_rate_cfd4)  !elapsed wall time for 1000 calls of cfd4 (seconds)
    error(2) = sum(abs(dfg-df4))/dble(n)

end subroutine test_fd_time
!---------------------------
end module fdmodule
















