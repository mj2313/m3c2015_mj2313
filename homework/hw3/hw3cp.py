"""Assess accuracy and perfomance of fortran finite difference module, fdmodule.
Converted to python module fd.so using f2py
"""    
    
from fd import fdmodule as f1 #you may change this line if desired
import numpy as np
import matplotlib.pyplot as plt

def fdtest_a(alpha=100.0):
    """alpha: input variable which sets width of 
    Guassian function to be differentiated
m: output two-element array, where the first element (m[0]) is such that the error with respect to the 2nd order centered finite difference (FD) scheme is proportional to the step-size dx to the power of m[0]. Similarly, the second element (m[1]) of m is such that the error with respect to the 4th order finite difference scheme is proportional to the step-size dx to the power of m[1]. The way the two elements of m are mathematically defined/estimated is by polynomial curve fitting (particularly linear fitting), that is a linear least-squares fit is applied to the log(error) vs. log(dx) curve, and then we take the coefficient of the linear term. If we worked with 2nd order centered FD scheme, this coefficient is m[0], if we worked with 4th order FD scheme, it is m[2]. This is basically the same approach as used in the previous m3c coursework (HW 2, Part 2, 3)."""

    N = np.array([100.,200,400,800,1600,3200,12800])
    DX = 2.0/(N-1)
    error = np.empty((2, len(N)))

    m = np.empty(2)

    for i in range(len(N))
        n = N[i]
        dx = DX[i]
        error[:,i] = test_fd(alpha,error)
    #Error wrt (dx) = constant * dx^n        = O(dx^n)


    plt.figure()
    plt.plot(N, error[0,:], color='blue', label="error(n) in a $2^{nd}$ order centered FD scheme", linestyle='-')  # blue 2nd order
    plt.plot(N, error[1,:], color='red', label="error(n) in a $4^{th}$ order FD scheme", linestyle='-')   # red  4th order
    plt.title('Testing the accuracy of ${\textit fdmodule}$ FD schemes with the Gaussian ($\alpha$=%s), Maros Janco, error vs. n'%(alpha))
    plt.xlabel('n')
    plt.ylabel('error')
    plt.show()
    # if necessary !!! plt.legend(bbox_to_anchor=(1,0.60), prop={'size':12})

    (m[0], est2) = np.polyfit(np.log(DX),np.log(error[0,:]),1)    # 2nd order
    (m[1], est2) = np.polyfit(np.log(DX),np.log(error[1,:]),1)    # 4th orders
    return error, m


   
def fdtest_at(alpha=100.0):
     """alpha: input variable which sets width of 
    Guassian function to be differentiated
    """ 

def fdtest_eff(#set as needed)        
    
    plt.savefig("hw3.png")
    
#This section will be used for assessment, you may enter a call to fdtest_eff, but
the three un-commented lines should be left as is in your final submission.    
if __name__ == "__main__":
    e1,m=fdtest_a(alpha=125.0)
    e2,m,t2 = fdtest_at()
    #add function call to fdtest_eff here if needed
    plt.show()
