!gfortran -o dgtsv_test.exe dgtsv_test.f90 -llapack
!./dgtsv_test.exe

program dgtsv_test
    implicit none
    real(kind=8), dimension(5) :: f  
    real(kind=8), dimension(size(f)):: df
    integer :: N,LDB,INFO,i,NRHS
    real(kind=8), allocatable, dimension(:,:) :: Atemp, x
    real(kind=8) :: dx
    real(kind=8), allocatable, dimension(:) :: DL, DU, D
    real(kind=8), allocatable, dimension(:,:) :: B, Btemp

    N = 5
    LDB = N
    !INFO ok
    !i later
    NRHS = 1

    f = (/ (dble(i), i=1,N) /)
    dx = 1.0

    allocate(B(LDB,NRHS),DL(N-1),D(N),DU(N-1))
    allocate(Atemp(N,N),Btemp(LDB,NRHS))

    DL = (/ (1.d0, i=1,N-1) /)
    DU = DL
    DU(1) = 2.d0
    DL(N-1) = 2.d0
    D = (/ (4.d0, i=1,N) /)
    D(1) = 1.d0
    D(N) = 1.d0

    print *,'DL=', DL
    print *,'D=', D
    print *,'DU=', DU
    print *,'f=', f
    print *, "break"

    B(1,1) = (-2.5d0*f(1) + 2.d0*f(2) + 0.5d0*f(3))/dx
    B(N,1) = (2.5d0*f(N) - 2.d0*f(N-1) - 0.5d0*f(N-2))/dx

    do i =2,N-1
     B(i,1) = 3.d0*(f(i+1)-f(i-1))/dx
    end do

    Btemp = B
    !B(:,1) = (/-2.0,2.0,-1.0,1.0,3.0/)

    Atemp(1,:) = (/1.0,2.0,0.0,0.0,0.0/)
    Atemp(2,:) = (/1.0,4.0,1.0,0.0,0.0/)
    Atemp(3,:) = (/0.0,1.0,4.0,1.0,0.0/)
    Atemp(4,:) = (/0.0,0.0,1.0,4.0,1.0/)
    Atemp(5,:) = (/0.0,0.0,0.0,2.0,1.0/)

    call dgtsv(N, NRHS, DL, D, DU, Btemp, LDB, INFO)
    print *, 'INFO=',INFO

    allocate(x(N,NRHS))
    x = Btemp(1:N,:)
    df = Btemp(:,1)
    print *, 'A=',Atemp(1,:)
    print *, 'A=',Atemp(2,:)
    print *, 'A=',Atemp(3,:)
    print *, 'A=',Atemp(4,:)
    print *, 'A=',Atemp(5,:)

    print *, 'x=',x
    print *, 'hsould be same as x=',df
    print *, 'b=',B
    print *, 'test:',matmul(Atemp,x)-B

end program dgtsv_test


