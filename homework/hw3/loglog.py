import numpy as np
import matplotlib.pyplot as plt

N = np.array([10.,100,1000,10000])
ERR = (N**2)

plt.figure()
plt.loglog(N, ERR, 'ro')
plt.plot(np.log(N), np.log(ERR), 'ro')


plt.show()
