program TEST
	implicit none
	!real(kind=8), dimension(3) :: D
	!real(kind=8), dimension(3,3) :: a
        real(kind=8), allocatable, dimension(:,:) :: ff, df1
        real(kind=8), dimension(3,2) :: df1_tout

	allocate(ff(2,3),df1(2,3))
        !f = reshape((/ 1,2,3,4,5,6/), (/2,3/))
	ff(1,:) = (/ 1,2,3/)
	ff(2,:) = (/ 4,5,6/)

        !call fun(df1_t,df1_tout)
        !print *, transpose(df1_tout) 
        !call fun(transpose(f) ,df1_tout)
        call fun(ff, df1)
	
        print *, df1 


end program TEST

subroutine fun(f,df1)
    implicit none
    real(kind=8), allocatable, dimension(:,:), intent(in) :: f
    real(kind=8), dimension(size(f,1),size(f,2)), intent(out) :: df1

    allocate(f(10,10))

    df1= 2*f

end subroutine fun


