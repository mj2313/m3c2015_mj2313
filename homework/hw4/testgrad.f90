program testgrad
 use fdmodule2d
 implicit none
    integer :: i1
    real(kind=8), allocatable, dimension(:) :: x1,x2 !coordinates stored in arrays
    real(kind=8), allocatable, dimension(:,:) :: x11,x22 
    real(kind=8), allocatable, dimension(:,:) :: f,df1,df2,df_amp,df_amp_exact !test function and results from grad
    real(kind=8) :: df_max



!gfortran -o testgrad.exe fdmodule.f90 hw41_dev.f90 testgrad.f90 -llapack 
!./testgrad.exe


    n1 = 1000
    n2 = 1000
allocate(x1(n1),x2(n2),x11(n2,n1),x22(n2,n1),f(n2,n1),df1(n2,n1),df2(n2,n1), &
    df_amp(n2,n1),df_amp_exact(n2,n1))

    dx1 = 0.5d0
    dx2 = 0.5d0

    do i1=1,n1
        x1(i1) = dble(i1-1)*dx1
    end do

    do i1=1,n2
        x2(i1) = dble(i1-1)*dx2
    end do

    do i1=1,n2
        x11(i1,:) = x1
    end do

    do i1=1,n1
        x22(:,i1) = x2
    end do

    f = sin(x11)*cos(x22)
    call grad(f,df1,df2,df_amp,df_max)
    print *, df1(500,500) - cos(dble(499)*dx1)*cos(dble(499)*dx1)

end program testgrad
