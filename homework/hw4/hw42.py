"""
Maros Janco, 00866730
Solve advection equation with odeint and fortran fd2 routine in fdmodule
(Convert fdmodule into f1.so: f2py -llapack -c fdmodule.f90 -m f1)
"""    
    
from fd import fdmodule as f1
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint



def advection1(tf,n,dx,c=1.0,S=0.0,display=False):
    """solve advection eqn, df/dt + c df/dx = S 
    where c and S are constants, x=0,dx,...(N-1)*dx,
    and f is periodic, f(x_i) = f(x_i+N*dx).
    Initial condition: f(x,t=0) = exp(-100.0*(x-x0)**2)
    Returns: f(x,tf)
    """
    #setting the x grid and initial condition fx0
    x = np.linspace(0,(n-1)*dx,n)
    x0 = (n-1)*dx/2.
    fx0 = np.exp(-100.0*(x-x0)**2)
    
    #setting variables for a fortran routine
    #(Note: as they remain constant during the RHS function,
    # I keep them outside for speedup)
    f1.n = n
    f1.dx = dx

    #function provides RHS to odeint
    def RHS(f,t,dx,n,c,S):
        dfx = f1.fd2(f) #derivative of f wrt x
        return S - c*dfx

    fxtf = odeint(RHS,fx0,[0,tf],args=(dx,n,c,S),mxstep = 50000)

    if display:
        plt.figure()
        plt.plot(x,fxtf[1])
        plt.plot(x,fxtf[0],'--')
        plt.xlabel('x')
        plt.ylabel('y')
        plt.title('Maros Janco, advection1, f(x,t) plotted is a solution to the linear advection\nequation with parameters S=%1.2f,c=%1.2f and t fixed, t=%1.2f'%(S,c,tf))
        plt.legend(['y = f(x,%1.2f)'%(tf),'y = f(x,0) (initial condition)'],loc='best') 
        plt.grid()

    return fxtf[1]
    
  
def test_advection1():
    tf=1.; c=1.; S=1.
    nvals = [20.,100.,200.,400.,800.,1600.,3200.] #range of values of n
    #(Note: we will get an asser if n is too small (exactly < 20) or too big, e.g. 6400)
    error = np.zeros(np.size(nvals)) #initialize L1 error

    #compute values of analytic solution with the same x grid as of advection1, considering its periodicity is n*dx = 1 (this is why I use the np.mod for the functional input (x-c*t) with divisor = periodicity = 1)
    
    def f_analytic(t,n,dx,c,S):
        x = np.linspace(0,(n-1)*dx,n)
	x0 = (n-1)*dx/2.
        return S*t+np.exp(-100.0*(np.mod(x-c*t,1.)-x0)**2)

    #compute L1 error for various values of n    
    for n in enumerate(nvals):
        dx = 1./n[1]
        f_est = advection1(tf,n[1],dx,c,S) # estimated solution by advection1 function
        error[n[0]] = sum(abs(f_est-f_analytic(tf,n[1],dx,c,S)))/float(n[1])

    #linear least squares fits for log(e) vs log(n)
    m,p = np.polyfit(np.log(nvals),np.log(error),1) #m is estimated convergence rate for advection1 function

        #by the previous homework 3, we know that fd2, i.e. 2nd-order centered finite difference scheme, has an analytic convergence rate of -2; thus:
    rate = abs(m+2)/2  #= abs((analytic_rate - computed_rate)/analytic_rate)
    assert rate <= 0.05, "Computed convergence rate is not within 5% of the analytical result"

     #plot illustrating the convergence rate
    plt.figure()
    plt.loglog(nvals,error,'*-')
    plt.title('Maros Janco, test_advection1, Convergance rate for advection1')
    plt.xlabel('n')
    plt.ylabel('L1 error')
    plt.axis('tight')
    plt.legend(['L1 error of advection1'],loc="best")   
    return m


#This section will be used for assessment, you may enter a call to fdtest_eff, but
#the three un-commented lines should be left as is in your final submission.    
if __name__ == "__main__":
    n = 1000
    dx = 1.0/float(n-1)
    f = advection1(2,n,dx,1,1,True)
    m = test_advection1()
    plt.show()
